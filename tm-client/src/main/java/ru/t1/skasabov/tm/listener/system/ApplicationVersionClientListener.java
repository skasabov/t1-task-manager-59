package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationVersionClientListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "version-client";

    @NotNull
    private static final String DESCRIPTION = "Show client program version.";

    @NotNull
    private static final String ARGUMENT = "-vc";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationVersionClientListener.command() == #consoleEvent.name " +
            "or @applicationVersionClientListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.println(getPropertyService().getApplicationVersion());
    }

}
