package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.listener.AbstractListener;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationArgumentListListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "arguments";

    @NotNull
    private static final String DESCRIPTION = "Show argument list.";

    @NotNull
    private static final String ARGUMENT = "-arg";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationArgumentListListener.command() == #consoleEvent.name " +
            "or @applicationArgumentListListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for (@NotNull final AbstractListener listener : listeners) {
            @Nullable final String argument = listener.argument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
