package ru.t1.skasabov.tm.listener.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.skasabov.tm.dto.request.ApplicationSystemInfoRequest;
import ru.t1.skasabov.tm.dto.response.ApplicationSystemInfoResponse;
import ru.t1.skasabov.tm.event.ConsoleEvent;

@Component
@NoArgsConstructor
public final class ApplicationSystemInfoListener extends AbstractSystemListener {

    @NotNull
    private static final String NAME = "info";

    @NotNull
    private static final String DESCRIPTION = "Show system information.";

    @NotNull
    private static final String ARGUMENT = "-i";

    @NotNull
    @Override
    public String argument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String command() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationSystemInfoListener.command() == #consoleEvent.name " +
            "or @applicationSystemInfoListener.argument() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final ApplicationSystemInfoRequest request = new ApplicationSystemInfoRequest();
        @NotNull final ApplicationSystemInfoResponse response = systemEndpoint.getSystemInfo(request);
        System.out.println("[INFO]");
        System.out.println("Available processors (cores): " + response.getAvailableProcessors());
        System.out.println("Free memory: " + response.getFreeMemory());
        System.out.println("Maximum memory: " + response.getMaximumMemory());
        System.out.println("Total memory: " + response.getTotalMemory());
        System.out.println("Usage memory: " + response.getUsageMemory());
    }

}
