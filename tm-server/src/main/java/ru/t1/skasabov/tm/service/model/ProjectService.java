package ru.t1.skasabov.tm.service.model;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.model.IProjectRepository;
import ru.t1.skasabov.tm.api.service.model.IProjectService;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.entity.UserNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.User;

import java.util.*;

@Service
@NoArgsConstructor
public class ProjectService extends AbstractUserOwnedService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) return;
        repository.removeAll(collection);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Project> addAll(@Nullable final Collection<Project> models) {
        if (models == null) return Collections.emptyList();
        repository.addAll(models);
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Collection<Project> set(@Nullable final Collection<Project> models) {
        if (models == null) return Collections.emptyList();
        repository.set(models);
        return models;
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ModelEmptyException();
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final Sort sortType) {
        if (sortType == null) return findAll();
        @NotNull List<Project> projects;
        switch(sortType) {
            case BY_CREATED:
                projects = repository.findAllSortByCreated();
                break;
            case BY_STATUS:
                projects = repository.findAllSortByStatus();
                break;
            case BY_NAME:
                projects = repository.findAllSortByName();
                break;
            default:
                projects = Collections.emptyList();
                break;
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        return repository.findOneByIndex(index);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOne(@Nullable final Project model) {
        if (model == null) throw new ModelEmptyException();
        repository.removeOne(model);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(id);
        if (model == null) return null;
        repository.removeOneById(id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final Project model = findOneByIndex(index);
        if (model == null) return null;
        repository.removeOneByIndex(index);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll() {
        repository.removeAll();
    }

    @Override
    @SneakyThrows
    public long getSize() {
        return repository.getSize();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sortType) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sortType == null) return findAll(userId);
        @NotNull List<Project> projects;
        switch(sortType) {
            case BY_CREATED:
                projects = repository.findAllSortByCreatedForUser(userId);
                break;
            case BY_STATUS:
                projects = repository.findAllSortByStatusForUser(userId);
                break;
            case BY_NAME:
                projects = repository.findAllSortByNameForUser(userId);
                break;
            default:
                projects = Collections.emptyList();
                break;
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        return repository.findOneByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project model = findOneById(userId, id);
        if (model == null) return null;
        repository.removeOneById(userId, id);
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Project model = findOneByIndex(userId, index);
        if (model == null) return null;
        repository.removeOneByIndex(userId, index);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        if (dateBegin == null || dateEnd == null) return create(userId, name, description);
        @Nullable final User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @NotNull final Project project = new Project();
        project.setUser(user);
        project.setName(name);
        project.setDescription(description);
        project.setDateBegin(dateBegin);
        project.setDateEnd(dateEnd);
        repository.add(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.update(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        repository.update(project);
        return project;
    }

}
