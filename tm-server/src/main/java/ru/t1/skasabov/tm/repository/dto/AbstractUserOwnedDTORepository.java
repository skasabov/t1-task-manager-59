package ru.t1.skasabov.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.skasabov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@NoArgsConstructor
public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

}
