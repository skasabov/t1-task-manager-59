package ru.t1.skasabov.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.skasabov.tm.api.repository.model.IRepository;
import ru.t1.skasabov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Repository
@NoArgsConstructor
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    @Transactional
    public void removeAll(@NotNull final Collection<M> models) {
        for (@NotNull final M model : models) {
            removeOne(model);
        }
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> addAll(@NotNull final Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (@NotNull final M model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @Override
    @Transactional
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return addAll(models);
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @Transactional
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
