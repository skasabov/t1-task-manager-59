package ru.t1.skasabov.tm.taskmanager.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import java.util.*;
import java.util.stream.Collectors;

public class ProjectDTORepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static UserDTO userOne;

    @NotNull
    private static UserDTO userTwo;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private ITaskDTORepository taskRepository;

    @NotNull
    private IProjectDTORepository projectRepository;

    @NotNull
    private IUserDTORepository userRepository;

    @Before
    @SneakyThrows
    public void initRepository() {
        userRepository = context.getBean(IUserDTORepository.class);
        userOne = new UserDTO();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        userTwo = new UserDTO();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        final long currentTime = System.currentTimeMillis();
        @NotNull final List<TaskDTO> tasks = new ArrayList<>();
        projectRepository = context.getBean(IProjectDTORepository.class);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            project.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) project.setStatus(Status.COMPLETED);
            else if (i < 7) project.setStatus(Status.IN_PROGRESS);
            else project.setStatus(Status.NOT_STARTED);
            if (i <= 5) project.setUserId(USER_ID_ONE);
            else project.setUserId(USER_ID_TWO);
            projectRepository.add(project);
            @NotNull final TaskDTO task = new TaskDTO();
            task.setName("Test " + i);
            if (i <= 5) task.setUserId(USER_ID_ONE);
            else task.setUserId(USER_ID_TWO);
            task.setProjectId(project.getId());
            tasks.add(task);
        }
        taskRepository = context.getBean(ITaskDTORepository.class);
        for (@NotNull final TaskDTO task : tasks) taskRepository.add(task);
    }

    @Test
    public void testUpdate() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_TWO).get(0);
        project.setName("Test Project One");
        project.setDescription("Test Description One");
        projectRepository.update(project);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(USER_ID_TWO, project.getId());
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(USER_ID_TWO, actualProject.getUserId());
        Assert.assertEquals("Test Project One", actualProject.getName());
        Assert.assertEquals("Test Description One", actualProject.getDescription());
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 4;
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_ONE);
            actualProjects.add(project);
        }
        projectRepository.addAll(actualProjects);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test Project " + i);
            project.setUserId(USER_ID_TWO);
            actualProjects.add(project);
        }
        taskRepository.removeAll();
        projectRepository.set(actualProjects);
        Assert.assertEquals(0, taskRepository.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
    }

    @Test
    public void testClearAll() {
        taskRepository.removeAll();
        projectRepository.removeAll();
        Assert.assertEquals(0, taskRepository.getSize());
        Assert.assertEquals(0, projectRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfTasks = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        taskRepository.removeAll(USER_ID_ONE);
        projectRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = projectRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        final long expectedNumberOfTasks = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll(USER_ID_TWO);
        taskRepository.removeAll(USER_ID_TWO);
        projectRepository.removeAll(projectList);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll();
        Assert.assertEquals(projectList.size(), projectRepository.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByName();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        actualProjects.add(1, actualProjects.get(NUMBER_OF_ENTRIES - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreated();
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final List<Status> statusList = projectRepository.findAllSortByStatus()
                .stream().map(ProjectDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            if (i < 4) actualStatuses.add(Status.COMPLETED);
            else if (i < 7) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<ProjectDTO> projectList = projectRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, projectList.size());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + (i + 5));
            actualProjects.add(project);
        }
        actualProjects.add(0, actualProjects.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualProjects.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final List<ProjectDTO> projectSortList = projectRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<ProjectDTO> actualProjects = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final ProjectDTO project = new ProjectDTO();
            project.setName("Test " + i);
            actualProjects.add(project);
        }
        Assert.assertEquals(actualProjects, projectSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final List<Status> statusList = projectRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(ProjectDTO::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindById() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final ProjectDTO actualProject = projectRepository.findOneById(USER_ID_ONE, projectId);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIdProjectNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(id));
    }

    @Test
    public void testFindByIdProjectNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(projectRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexProjectNotFound() {
        Assert.assertNull(projectRepository.findOneByIndex(NUMBER_OF_ENTRIES));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final ProjectDTO project = projectRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final ProjectDTO actualProject = projectRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualProject);
        Assert.assertEquals(project.getName(), actualProject.getName());
        Assert.assertEquals(project.getDescription(), actualProject.getDescription());
        Assert.assertEquals(project.getUserId(), actualProject.getUserId());
    }

    @Test
    public void testFindByIndexForUserProjectNotFound() {
        Assert.assertNull(projectRepository.findOneByIndex(USER_ID_ONE, NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = projectRepository.getSize() + 1;
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_ONE);
        projectRepository.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setName("Test Project");
        project.setUserId(USER_ID_TWO);
        projectRepository.add(project);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = projectRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(invalidId));
        Assert.assertTrue(projectRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(projectRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(projectRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final ProjectDTO project = projectRepository.findAll().get(0);
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        taskRepository.removeAll(taskRepository.findAllByProjectId(project.getId()));
        projectRepository.removeOne(project);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = projectRepository.getSize() - 1;
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        taskRepository.removeAll(taskRepository.findAllByProjectId(projectId));
        projectRepository.removeOneById(projectId);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String projectId = projectRepository.findAll(USER_ID_ONE).get(0).getId();
        final long expectedNumberOfTasks = taskRepository.getSize(USER_ID_ONE) - 1;
        taskRepository.removeAll(taskRepository.findAllByProjectId(projectId));
        projectRepository.removeOneById(USER_ID_ONE, projectId);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize(USER_ID_ONE));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = projectRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        @NotNull final String projectId = projectRepository.findAll().get(0).getId();
        taskRepository.removeAll(taskRepository.findAllByProjectId(projectId));
        projectRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = projectRepository.getSize(USER_ID_TWO) - 1;
        final long expectedNumberOfTasks = taskRepository.getSize(USER_ID_TWO) - 1;
        @NotNull final String projectId = projectRepository.findAll(USER_ID_TWO).get(0).getId();
        taskRepository.removeAll(taskRepository.findAllByProjectId(projectId));
        projectRepository.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize(USER_ID_TWO));
        Assert.assertEquals(expectedNumberOfEntries, projectRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testFindAllByEmptyUsers() {
        @NotNull final List<UserDTO> users = new ArrayList<>();
        Assert.assertEquals(0, projectRepository.findAllByUsers(users).size());
    }

    @Test
    public void testFindAllByUsers() {
        @NotNull final List<UserDTO> users = new ArrayList<>(Arrays.asList(userOne, userTwo));
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.findAllByUsers(users).size());
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        taskRepository.removeAll();
        projectRepository.removeAll();
        userRepository.removeAll();
    }

}
